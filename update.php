<?php
include "./vendor/autoload.php";
use pondit\seip\Model\Student;
if (isset($_SERVER["REQUEST_METHOD"]) != "POST") {
    echo "Only Get Method  allowed";
    die();
}else {
   $id = $_GET['id'];
   $students = new Student;
   $studentInfo = $students->update($_POST, $id);
    header('Location:./index.php');
    // print_r($_POST);
}

?>