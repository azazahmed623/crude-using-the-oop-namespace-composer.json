<?php
include "header.php";
?>
<div class="container-fluid pt-5">
  <nav aria-label="breadcrumb">
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="index.php">Product</a></li>
      <li class="breadcrumb-item active" aria-current="page">Add Product</li>
    </ol>
  </nav>
</div>
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="form-group">
        <form action="./store.php" method="POST">
          <table>
            <tr>
              <label for="id">No:</label>
              <input type="text" id="id" class="form-control" name="id">
            </tr>
            <tr>
              <label for="name">Product Name:</label>
              <input type="text" id="name" class="form-control" name="name">
            </tr>
            <tr>
              <label for="class">Category:</label>
              <input type="text" id="category" class="form-control" name="category">
            </tr>
            <tr>
              <label for="class">price:</label>
              <input type="text" id="price" class="form-control" name="price">
            </tr>
            <tr>
              <button type="submit" class="btn csbt btn-success mt-3 px-4">Save</button>
            </tr>
          </table>
        </form>
      </div>
    </div>
  </div>
</div>

<!-- Optional JavaScript; choose one of the two! -->

<!-- Option 1: Bootstrap Bundle with Popper -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

<!-- Option 2: Separate Popper and Bootstrap JS -->
<!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
    -->
</body>

</html>