<?php 
include "./vendor/autoload.php";
session_unset();

use pondit\seip\Model\Student;
if (isset($_SERVER["REQUEST_METHOD"]) != "POST") {
    echo "Only post Method allowed";
    die();
}else {

    $students = new Student;
    $students->store($_POST);
    header('Location:./index.php');
    // print_r($_POST);
}

?>