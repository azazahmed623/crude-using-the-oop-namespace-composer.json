<?php
include "header.php";
include "./vendor/autoload.php";
use pondit\seip\Model\Student;
if (isset($_SERVER["REQUEST_METHOD"]) != " GET") {
    echo "Only Get Method  allowed";
    die();
}else {
   $id =  $_GET['id'];
   $students = new Student;
   $studentInfo  =  $students->show($id);

}

?>
<div class="container-fluid pt-5">
  <nav aria-label="breadcrumb">
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="index.php">Product</a></li>
      <li class="breadcrumb-item active" aria-current="page">Show Product</li>
    </ol>
  </nav>
</div>

    <div class="container">
    
    <table class="table table-bordered border-primary">
        <tr>
            <th>No:</th>
            <th>Product Name:</th>
            <th>Category:</th>
            <th>Price:</th>
        </tr>
        <tr>
            <td><?=$studentInfo['id']?></td>
            <td><?=$studentInfo['name']?></td>
            <td><?=$studentInfo['category']?></td>
            <td><?=$studentInfo['price']?></td>
        </tr>
    </table>
    </div>

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
    -->
  </body>
</html>