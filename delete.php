<?php

$id =  $_GET['id'];

include "./vendor/autoload.php";
use pondit\seip\Model\Student;
if (isset($_SERVER["REQUEST_METHOD"]) != " GET") {
    echo "Only Get Method  allowed";
    die();
}else {

    $students = new Student;
    $students->destroy($id);
    header('Location:./index.php');
    // print_r($_POST);
}

?>