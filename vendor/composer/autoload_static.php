<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInite3f0fc36e6ed6105c56961de5b3de3d2
{
    public static $prefixLengthsPsr4 = array (
        'p' => 
        array (
            'pondit\\seip\\' => 12,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'pondit\\seip\\' => 
        array (
            0 => __DIR__ . '/../..' . '/src',
        ),
    );

    public static $classMap = array (
        'Composer\\InstalledVersions' => __DIR__ . '/..' . '/composer/InstalledVersions.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInite3f0fc36e6ed6105c56961de5b3de3d2::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInite3f0fc36e6ed6105c56961de5b3de3d2::$prefixDirsPsr4;
            $loader->classMap = ComposerStaticInite3f0fc36e6ed6105c56961de5b3de3d2::$classMap;

        }, null, ClassLoader::class);
    }
}
