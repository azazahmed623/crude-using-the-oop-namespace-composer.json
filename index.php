<?php
include "header.php";
include "./vendor/autoload.php";

session_unset();

use pondit\seip\Model\Student;

$students = new Student;
$students = $students->getData();

// print_r($students);
// die();

// session_unset();
?>
<div class="container-fluid pt-5">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="index.php">Product</a></li>
            <li class="breadcrumb-item active" aria-current="page">Product-list</li>
        </ol>
    </nav>
</div>
<div class="container-fluid">
        <div class="row">
          <div class="col-md-6">
            <div class="cat-list-left">
              <h2>Product List</h2>
            </div>
          </div>
          <div class="col-md-6">
            <div class="cat-list-right">
              <a href="./create.php">Add Product</a>
            </div>
          </div>
        </div>
      </div>
<div class="container">
    <!-- <div class="d-flex flex-row-reverse">
        <a href="./create.php" type="button" class="btn btn-success">Add</a>
    </div> -->
    <?php

    if (isset($_SESSION['massege'])) { ?>
        <div class="alert alert-success" role="alert">
            <?= $_SESSION['massege'] ?>
        </div>
        <?php
        unset($_SESSION['massege']);
        ?>
    <?php } ?>
    <div class="row">
        <div class="col-md-12">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>SL</th>
                        <th>Product Name</th>
                        <th>Category</th>
                        <th>Price</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    foreach ($students as $key => $student) { ?>
                        <tr>
                            <td><?= $student['id'] ?></td>
                            <td><?= $student['name'] ?></td>
                            <td><?= $student['category'] ?></td>
                            <td><?= $student['price'] ?></td>
                            <td>
                                <a href="./show.php?id=<?= $student['id'] ?>" class="btn btn-info">Show</a> 
                                <a href="./edit.php?id=<?= $student['id'] ?>" class="btn btn-warning">Edit</a> 
                                <a href="./delete.php?id=<?= $student['id'] ?>" class="btn btn-danger">Delete</a>
                            </td>
                        </tr>
                    <?php  } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<!-- Optional JavaScript; choose one of the two! -->

<!-- Option 1: Bootstrap Bundle with Popper -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

<!-- Option 2: Separate Popper and Bootstrap JS -->
<!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
    -->
</body>

</html>