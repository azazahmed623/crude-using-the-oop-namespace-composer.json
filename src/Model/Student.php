<?php 
namespace pondit\seip\Model;
session_unset();
class Student{
    public function __construct()
    {
        session_start();
    }

    public function getData()
    {
       
        return  $_SESSION['students'];
        
    }

    public function store($requestFormData)
    {
     
       $_SESSION['students'][] = $requestFormData;

       $_SESSION['massege'] = "Successfully Created !";
      
    }

    public function show($id)
    {
       
        $students =  $_SESSION['students'];
        // print_r($students);
        // die();
        $studentInfo = null;
        foreach ($students as $key => $student) {
            if($student['id'] == $id){
            $studentInfo = $_SESSION['students'][$key];
            return $studentInfo;
            }
        
        }
        
    }
    public function update($data, $id)
    {
        $students = $_SESSION['students'];
        // print_r($students);
        // die();
        foreach ($students as $key => $student) {
            if($student['id'] == $id){
                $_SESSION['students'][$key] = $data;
            }
        }
        $_SESSION['massege'] = "Successfully Updated !";
    }

    public function destroy($id)
    {
        $students =  $_SESSION['students'];

        foreach ($students as $key => $student) {
            if($student['id'] == $id){
            unset($_SESSION['students'][$key]);
            }
        
        }

        $_SESSION['massege'] = "Successfully Deleted !";
    }
}

?>